# Problem

Comparison of branches in GitLab appear to only work in a specific way round - is this misleading UX?

# Test

Make some changes on a feature branch: 

![changes exist](./media/Graph.PNG)

Comparison of the same branches only works one way round:

![first comparison](./media/compare1.PNG)

![second comparison](./media/compare2.PNG)